.PHONY: all clean test

all: build/mem.o build/util.o build/mem_debug.o build/tests.o build/main.o
	gcc $^ -o build/main

build:
	mkdir -p build

build/%.o: src/%.c build
	gcc --std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG $< -c

clean: 
	rm -rf build